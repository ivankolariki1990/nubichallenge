import { create } from "zustand";
import { Alert } from "react-native";
import * as SecureStore from "expo-secure-store";

const useStore = create((set, get) => ({
  dineroDisponible: 100, // Valor inicial por defecto

  // Inicializar el dinero disponible desde SecureStore
  inicializarDineroDisponible: async () => {
    const dineroGuardado = await SecureStore.getItemAsync("dineroDisponible");
    if (dineroGuardado) {
      set({ dineroDisponible: parseFloat(dineroGuardado) });
    }
  },

  // Actualizar y guardar el dinero disponible
  setDineroDisponible: async (monto) => {
    const nuevoTotal = get().dineroDisponible + monto;
    await SecureStore.setItemAsync("dineroDisponible", nuevoTotal.toString());
    set({ dineroDisponible: nuevoTotal });
  },

  retirarDinero: async (monto) => {
    const estadoActual = get();
    if (monto > estadoActual.dineroDisponible) {
      Alert.alert(
        "Error",
        "No puedes retirar más de lo que tienes disponible."
      );
      return;
    }
    const nuevoTotal = estadoActual.dineroDisponible - monto;
    await SecureStore.setItemAsync("dineroDisponible", nuevoTotal.toString());
    set({ dineroDisponible: nuevoTotal });
  },
}));

export default useStore;
