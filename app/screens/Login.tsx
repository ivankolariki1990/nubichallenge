import React, { useState } from "react";
import { StyleSheet, TextInput, View, Button, Text } from "react-native";
import { useAuth } from "../context/AuthContext";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { login } = useAuth();

  const handleSubmit = async () => {
    try {
      await login();
    } catch (error) {
      console.error("Error en el login", error);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.hola}>¡HOLA! </Text>
      <View style={styles.form}>
        <View style={styles.inputView}>
          <TextInput
            style={styles.input}
            placeholder="Email"
            onChangeText={(text: string) => setEmail(text)}
            value={email}
          />
          <TextInput
            style={styles.input}
            placeholder="Password"
            secureTextEntry={true}
            onChangeText={(text: string) => setPassword(text)}
            value={password}
          />
        </View>
        <Button onPress={handleSubmit} title="Login" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  hola: {
    top: "15%",
    fontSize: 30,
    color: "white",
  },
  form: {
    width: "100%",
    height: "80%",
    backgroundColor: "white",
    marginTop: "40%",
    borderRadius: 40,
  },
  inputView: {
    marginTop: "20%",
  },
  input: {
    height: 44,
    borderWidth: 1,
    borderRadius: 4,
    padding: 10,
    backgroundColor: "transparent",
    margin: "5%",
  },
  container: {
    width: "100%",
    backgroundColor: "blue",
  },
});

export default Login;
