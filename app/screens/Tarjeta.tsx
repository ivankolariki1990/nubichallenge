import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { useAuth } from "../context/AuthContext";
import TarjetaOculta from "../components/TarjetaGiratoria";

export default function Tarjeta({ navigation }) {
  const { userData } = useAuth();
  const handleBloquearTarjeta = () => {
    alert("Tarjeta bloqueada");
  };

  const handleVerMovimientos = () => {
    navigation.navigate("Actividad");
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Tu Tarjeta</Text>
      <TarjetaOculta />
      <View style={styles.cardDetails}>
        <Text style={styles.cardDetailText}>
          Número de Tarjeta: XXXX-XXXX-XXXX-1234
        </Text>
        <Text style={styles.cardDetailText}>Válido hasta: 12/24</Text>
        <Text style={styles.cardDetailText}>
          Nombre: {userData?.name} {userData?.lastName}
        </Text>
      </View>

      {/* Botones para acciones */}
      <TouchableOpacity style={styles.button} onPress={handleBloquearTarjeta}>
        <Text style={styles.buttonText}>Bloquear Tarjeta</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button} onPress={handleVerMovimientos}>
        <Text style={styles.buttonText}>Ver Movimientos</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
    backgroundColor: "#fff",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  cardDetails: {
    marginBottom: 20,
  },
  cardDetailText: {
    fontSize: 16,
    color: "#333",
    marginBottom: 5,
  },
  button: {
    backgroundColor: "#007bff",
    padding: 15,
    borderRadius: 5,
    marginVertical: 10,
    width: "100%",
  },
  buttonText: {
    color: "#fff",
    fontSize: 16,
    textAlign: "center",
  },
});
