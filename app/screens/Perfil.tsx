import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { useAuth } from "../context/AuthContext"; // Asegúrate de importar correctamente el hook

export default function Perfil() {
  const { userData, logout } = useAuth(); // Obtener los datos del usuario del contexto

  return (
    <View style={styles.container}>
      <Text style={styles.titulo}>Perfil</Text>
      <Text style={styles.nombre}>
        {userData?.name} {userData?.lastName}
      </Text>
      <Text style={styles.email}>{userData?.email}</Text>
      <Button title="Cerrar Sesión" onPress={logout} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  titulo: {
    fontSize: 22,
    fontWeight: "bold",
  },
  nombre: {
    fontSize: 20,
    marginTop: 8,
  },
  email: {
    fontSize: 18,
    color: "gray",
    marginTop: 4,
  },
});
