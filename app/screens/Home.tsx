import React, { useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { ActionModal } from "../components/Modal";
import { useAuth } from "../context/AuthContext";
import useStore from "../utils/Store";
import Services from "../services/Services";

export default function Home() {
  const dineroDisponible = useStore((state) => state.dineroDisponible);
  const [modalVisible, setModalVisible] = useState(false);
  const [actionType, setActionType] = useState(null);

  const { userData } = useAuth();

  const handleOpenModal = (type: string) => {
    setActionType(type);
    setModalVisible(true);
  };
  return (
    <View style={styles.container}>
      <ActionModal
        visible={modalVisible}
        onClose={() => setModalVisible(false)}
        actionType={actionType}
      />
      <Text style={styles.nombre}>
        Hola {userData?.name} {userData?.lastName}, tu saldo es
      </Text>
      <Text style={styles.dineroDisponible}>${dineroDisponible}</Text>
      <View style={styles.row}>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.button2}
            onPress={() => handleOpenModal("cargar")}
          />
          <Text style={styles.buttonLabel}>Carga Plata +</Text>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.button2}
            onPress={() => handleOpenModal("retirar")}
          />
          <Text style={styles.buttonLabel}>Retirar Plata -</Text>
        </View>
      </View>
      <Services />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EBE9E8",
  },
  nombre: {
    color: "#000000",
    fontSize: 20,
    textAlign: "center",
  },
  dineroDisponible: {
    color: "#000000",
    fontSize: 30,
    textAlign: "center",
  },

  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    width: "95%",
    marginBottom: 20,
    backgroundColor: "#ffffff",
  },
  buttonContainer: {
    alignItems: "center",
  },

  button2: {
    backgroundColor: "#007bff",
    padding: 10,
    width: "180%",
    height: "20%",
    borderRadius: 5,
  },
  buttonLabel: {
    color: "#000",
    marginTop: 5,
  },
});
