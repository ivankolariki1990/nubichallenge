import React from "react";
import { StyleSheet, Text, View, FlatList } from "react-native";
import { useAuth } from "../context/AuthContext"; // Asegúrate de importar correctamente el hook

export default function Actividad() {
  const { userData } = useAuth(); // Obtener los datos del usuario del contexto

  // Renderiza cada ítem de la lista
  const renderItem = ({ item }) => {
    const amount = item["amount "];
    const isPositiveAmount =
      typeof amount === "string" && amount.startsWith("+");

    return (
      <View style={styles.itemContainer}>
        {/* Columna izquierda para título y fecha */}
        <View style={styles.leftColumn}>
          <Text style={styles.tipo}>{item.title}</Text>
          <Text style={styles.fecha}>{item.date}</Text>
        </View>

        {/* Columna derecha para el monto */}
        <Text
          style={
            isPositiveAmount ? styles.cantidadPositiva : styles.cantidadNegativa
          }
        >
          {amount}
        </Text>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titulo}>Movimientos</Text>
      <FlatList
        data={userData?.movements} // Usar los movimientos de la API
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()} // Usar el índice como key si no tienes un ID único
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: "#fff",
  },
  titulo: {
    fontSize: 22,
    fontWeight: "bold",
    marginTop: 30,
    textAlign: "center",
  },
  itemContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#eee",
  },
  leftColumn: {
    // Columna izquierda para título y fecha
    flex: 1, // Toma el espacio disponible
  },
  tipo: {
    fontSize: 16,
    fontWeight: "500",
  },
  fecha: {
    fontSize: 14,
    color: "#666",
  },
  cantidadPositiva: {
    fontSize: 16,
    fontWeight: "500",
    color: "green",
  },
  cantidadNegativa: {
    fontSize: 16,
    fontWeight: "500",
    color: "red",
  },
  // ... cualquier otro estilo que necesites ...
});
