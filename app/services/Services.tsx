import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import React, { useState } from "react";
import { useAuth } from "../context/AuthContext";
import { ActionModal } from "../components/Modal";

export default function Services() {
  const { userData } = useAuth();

  const [modalVisible, setModalVisible] = useState(false);
  const [actionType, setActionType] = useState(null);

  if (!userData || !userData.services) {
    // Renderiza un mensaje de carga o un componente vacío si no hay datos
    return <Text>Cargando servicios...</Text>;
  }

  const filaUno = userData?.services.slice(0, 3);
  const filaDos = userData?.services.slice(3, 6);

  const handleOpenModal = (type: string) => {
    setActionType(type);
    setModalVisible(true);
  };

  const tiposDeAccionPorServicio = {
    "Pedir Plata": "pedir",
    "Mandar plata": "mandar",
    "Cargar la SUBE": "sube",
    "Recargar un celular": "recargar",
    "Pagar tus servicios": "pagar",
    // Agrega más mapeos según sea necesario
  };

  const renderFilaDeServicios = (servicios) => {
    return servicios.map((servicio, index) => {
      const tipoDeAccion = tiposDeAccionPorServicio[servicio];

      return (
        <View key={index} style={styles.servicioContainer}>
          <ActionModal
            visible={modalVisible}
            onClose={() => setModalVisible(false)}
            actionType={actionType}
          />
          <TouchableOpacity
            style={styles.servicioButton}
            onPress={() => handleOpenModal(tipoDeAccion)}
          >
            <Text style={styles.servicioButtonText}>{servicio}</Text>
          </TouchableOpacity>
        </View>
      );
    });
  };

  return (
    <View style={styles.serviciosWrapper}>
      <View style={styles.serviciosFila}>{renderFilaDeServicios(filaUno)}</View>
      <View style={styles.serviciosFila}>{renderFilaDeServicios(filaDos)}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  serviciosWrapper: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
  },
  serviciosFila: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    backgroundColor: "white",
  },
  servicioContainer: {
    margin: 5,
    flex: 1,
    backgroundColor: "white",
  },
  servicioButton: {
    backgroundColor: "#007bff",
    padding: 10,
    borderRadius: 5,
    alignItems: "center",
  },
  servicioButtonText: {
    color: "#fff",
    fontSize: 12,
  },
});
