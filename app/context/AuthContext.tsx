import React, { createContext, useContext, useState, useEffect } from "react";
import axios from "axios";
import * as SecureStore from "expo-secure-store";

type AuthContextType = {
  userData: any;
  isAuthenticated: boolean;
  login: () => Promise<void>;
  logout: () => Promise<void>;
};

const AuthContext = createContext<AuthContextType>({
  userData: null,
  isAuthenticated: false,
  login: async () => {},
  logout: async () => {},
});

export const AuthProvider = ({ children }) => {
  const [userData, setUserData] = useState(null);
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  useEffect(() => {
    // Recuperar los datos del usuario al iniciar la aplicación
    getUserData().then((data) => {
      if (data) {
        setUserData(data);
      }
    });
  }, []);

  const login = async () => {
    try {
      const response = await axios.get(
        "https://nubi-challenge.wiremockapi.cloud/login"
      );
      const userData = response.data;
      console.log("Datos del usuario:", userData);

      // Guardar datos del usuario
      await saveUserData(userData);
      setUserData(userData);

      return userData;
    } catch (error) {
      console.error("Error durante el login", error);
      throw error;
    }
  };

  async function saveUserData(userData: string) {
    const userDataString = JSON.stringify(userData);
    await SecureStore.setItemAsync("userData", userDataString);
    setIsAuthenticated(true);
  }

  async function getUserData() {
    const userDataString = await SecureStore.getItemAsync("userData");
    if (userDataString) {
      const data = JSON.parse(userDataString);
      setIsAuthenticated(true); // Establece isAuthenticated a true aquí
      return data;
    }
    return null;
  }

  const logout = async () => {
    await SecureStore.deleteItemAsync("userData"); // Limpiar datos del almacenamiento seguro
    setUserData(null); // Limpiar los datos del usuario en el estado
    setIsAuthenticated(false); // Establecer que el usuario no está autenticado
  };

  const value = {
    userData,
    isAuthenticated,
    login,
    logout,
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export const useAuth = () => {
  return useContext(AuthContext);
};
