import React, { useState } from "react";
import { Alert, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { useAuth } from "../context/AuthContext"; // Asegúrate de importar correctamente el hook

export default function TarjetaOculta() {
  const { userData } = useAuth(); // Obtener los datos del usuario del contexto

  const [datosVisibles, setDatosVisibles] = useState(false);
  const PIN_CORRECTO = "12345";

  const solicitarPINyMostrarDatos = () => {
    Alert.prompt(
      "Desbloquear Datos",
      "Introduce el PIN para ver los detalles",
      [
        {
          text: "Cancelar",
          style: "cancel",
        },
        {
          text: "Confirmar",
          onPress: (pin) => {
            if (pin === PIN_CORRECTO) {
              setDatosVisibles(true);
            } else {
              Alert.alert("Error", "PIN incorrecto");
            }
          },
        },
      ],
      "secure-text" // Este tipo hace que el texto ingresado sea ocultado como en los campos de contraseña
    );
  };

  return (
    <TouchableOpacity onPress={solicitarPINyMostrarDatos} style={styles.card}>
      <Text style={styles.cardTitle}>Tarjeta Virtual</Text>
      {datosVisibles ? (
        <View style={styles.cardDetails}>
          <Text style={styles.cardDetailText}>Número: 1234 5678 9012 3456</Text>
          <Text style={styles.cardDetailText}>Válido hasta: 12/24</Text>
          <Text style={styles.cardDetailText}>
            Titular: {userData?.name} {userData?.lastName}
          </Text>
        </View>
      ) : (
        <View style={styles.cardDetails}>
          <Text style={styles.cardDetailText}>Número: XXXX XXXX XXXX 3456</Text>
          <Text style={styles.cardDetailText}>Válido hasta: XX/XX</Text>
          <Text style={styles.cardDetailText}>Titular: XXXXX XXXX</Text>
        </View>
      )}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: "#007bff",
    borderRadius: 10,
    padding: 20,
    alignItems: "center",
    margin: 10,
  },
  cardTitle: {
    fontSize: 22,
    color: "#fff",
    marginBottom: 15,
  },
  cardDetails: {
    alignItems: "center",
  },
  cardDetailText: {
    fontSize: 16,
    color: "#fff",
    marginVertical: 5,
  },
});
