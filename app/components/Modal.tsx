import React, { useState } from "react";
import {
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Alert,
} from "react-native";
import useStore from "../utils/Store";

export const ActionModal = ({ visible, onClose, actionType }) => {
  const [inputValue, setInputValue] = useState("");
  const setDineroDisponible = useStore((state) => state.setDineroDisponible);
  const retirarDinero = useStore((state) => state.retirarDinero);

  const handleConfirm = () => {
    const monto = parseFloat(inputValue);
    if (isNaN(monto) || monto <= 0) {
      Alert.alert("Error", "Por favor ingresa un monto válido.");
      return;
    }

    switch (actionType) {
      case "cargar":
        setDineroDisponible(monto);
        break;
      case "retirar":
        retirarDinero(monto);
        break;
      case "mandar":
        // Lógica para mandar dinero
        break;
      case "pedir":
        // Lógica para pedir dinero
        break;
      case "sube":
        // Lógica para cargar la SUBE
        break;
      case "recargar":
        // Lógica para recargar un celular
        break;
      case "pagar":
        // Lógica para pagar servicios
        break;
      // Añade más casos según sea necesario
    }

    resetAndClose();
  };

  const resetAndClose = () => {
    setInputValue(""); // Resetear el input
    onClose(); // Cierra el modal
  };

  const getContent = () => {
    switch (actionType) {
      case "cargar":
        return "Cuanto dinero deseas cargar?";
      case "retirar":
        return "Cuanto dinero deseas retirar?";
      case "mandar":
        return "Cuanto dinero deseas enviar?";
      case "pedir":
        return "Cuanto dinero deseas pedir?";
      case "sube":
        return "Cuanto dinero deseas cargar en tu SUBE?";
      case "recargar":
        return "Cuanto dinero deseas recargar en tu celular?";
      case "pagar":
        return "Cuanto dinero deseas utilizar para pagar servicios?";
      default:
        return "Acción no reconocida";
    }
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={visible}
      onRequestClose={resetAndClose}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>{getContent()}</Text>
          <TextInput
            style={styles.input}
            onChangeText={setInputValue}
            value={inputValue}
            placeholder="Ingresa un monto"
            keyboardType="numeric"
          />
          <TouchableOpacity
            style={styles.buttonConfirm}
            onPress={handleConfirm}
          >
            <Text style={styles.textStyle}>Confirmar</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonClose} onPress={resetAndClose}>
            <Text style={styles.textStyle}>Cerrar</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    marginTop: "90%",
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    height: "60%",
    width: "100%",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    marginTop: 10,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: "80%", // Ajusta el ancho según tus necesidades
    borderRadius: 5,
  },
  buttonConfirm: {
    backgroundColor: "green",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    marginTop: 10,
  },
});
